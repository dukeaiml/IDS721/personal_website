+++
date = "2022-08-14T00:00:00-05:00"
title = "Statistical Genetics"
description = "Mixed Effect Neural Networks for Multi-Trait Genomic Prediction Including Intermediate Omics Data"
slug = "biostat-1"
draft = false

[taxonomies]
    categories = ["projects"]
    tags = ["zola"]

[extra]
    page_identifier = "projects-biostat-1"
+++

Project: Mixed Effect Neural Networks for Multi-Trait Genomic Prediction Including Intermediate Omics Data

<!-- more -->

<iframe src="https://drive.google.com/file/d/1_-ZkY2nD-YoArdmdrhqsYgRn_XJlPbvk/preview" title="Ataraxia mockup" style="width:100%; height:500px;" frameborder="0"></iframe>

