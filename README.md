[![pipeline status](https://gitlab.com/dukeaiml/IDS721/zhankai_ye_mini_project_3/-/wikis/uploads/8b9be60c3dabf89331bc0c118cae1e44/pipeline.svg)](https://gitlab.com/dukeaiml/IDS721/zhankai_ye_individual_project_1/-/commits/main)

# IDS721 Individual Project 1: Hosted Personal Website

This project is an extension of the mini-project 1 which focuses on the development and automated deployment of a personal website, utilizing Zola and the Rust programming language. The website is also hosted on Netlify. This project serves as a curated platform that displays some of my selected projects from courses and research. 

## Goals
* Website built with ZolaLinks to an external site., Hugo, Gatsby, Next.js or equivalent
* GitLab workflow to build and deploy site on push
* Hosted on Vercel, Netlify, AWS Amplify, AWS S3, or others.

## Steps
1. Install Zola
2. Create website with zola using `zola init <YOUR_WEB_NAME>`.
3. Choose a themem and then download it to the theme directory
```
cd <YOUR_WEB_NAME>/themes
git clone <GIT_THEME_URL>
```
3. Modify the `config.toml` file, along with the content pages in the `content` to design the website according to the requirement.
4. Build the project using `zola build`.
5. Test using `zola serve`.
6. Host on Gitlab: Setting up the GitLab CI/CD pipeline by creating and configure the `.gitlab-ci.yml` file in the root directory of the repository.
7. Host on Netlify: Create and configure the `netlify.toml` file in the root directory of the project. Log in to Netlify, link and deploy the project.

## Demo
https://zhankai-ye.netlify.app/

![Video Demo](https://gitlab.com/dukeaiml/IDS721/personal_website/-/wikis/uploads/7f6c41bb500e09ed8e419f6cb07354bc/demo.mov)

* Website
![wb](https://gitlab.com/dukeaiml/IDS721/personal_website/-/wikis/uploads/5823b2fe280323b7654fe65a776b119e/website.png)
* Gitlab
![gl](https://gitlab.com/dukeaiml/IDS721/personal_website/-/wikis/uploads/c293031f3ceb4108fb09fd54b57ef21f/gitlab.png)
![ny](https://gitlab.com/dukeaiml/IDS721/personal_website/-/wikis/uploads/bfdbffc49563793bdf7728d870f58c76/netlify.png)

